# Route Individual Levels

`This Color` means the Autosplitter reacts on this by default3
## Dreamertown
_MumSkip_

-> Dreamertown: silverKey

-> House: Books

-> Library: _read_, ShadeFern

-> School: complete Quest ShadeFern

-> House: Rose

-> Inn: _quest_, _shashTalk_

-> Larissas Mum: Sandwitch

-> Dreamertown: _talk_

-> WineTrader: Wine

-> Dreamertown: complete quest Rose, Balloon, complete Quest `Balloon`

-> Inn: complete Quest Wine

-> Dreamertown: trigger WellCutscene

-> `Rissen`: Compass (don't talk to rissen again)

-> RatGuy: _fight_, SleepingDraught

-> Dreamertown: Mum, `End`

## Steamertown
-> SleeperHouse: Cork, OilCan

-> SteamerTown: Knock Fathers Door, Cat, use Cork

-> Ol'Raake's: Cork, Rizinus

-> Steamertown: use Cork

-> Tavern: Cork, Cork, Stew

-> Steamertown: use Cork

-> Hospital: Cork

-> WorkingShop: Sword, Dynamite

-> `Mine`

-> DO MINE STUFF

-> `Steamertown`

-> Smith: give Zantium

-> SteamerTown: use Cork

-> SW into `Crypt`: IB in BoxSolutionRoom

-> SW down: big circle clockwise, IB into BoxRoom

-> SW down: Ankh, fight, key

-> up: key

-> down

-> SW up, Leave `Crypt`

-> Fathers': read

-> SteamerPark: use Cork, use Cork, win Wheel, use Oil, fill water 3-1, even Water, `End`

## Hive

Button 1, door 1, button 2, door 2

->  SW ontop of door in Level 2: jump off (or solve box-riddle), doubleSpawn to the left, jump up the wall, button, door, talk to dad

-> SW to exit of Level 3

-> Level 4: WarMace, Riddle: extra-round and skip button 2, talk to shach, tripple-door buttons

-> Level 5: button, door, button, door, bomb wall, go OOB, go to exit

-> level 6: go back and SW: go to exit

-> Level 7: Lever to deactivate cristal, enter Level 6, SW back to level 7, OOB to the green cristal

-> Level 8: left room button (bridge 2), right room button (bridge 1)

-> Level 9: Lever, Lever, middle lever, lever, lever, button

-> SW into Level 10: go north, skip endboss, `End`

## FairyForest
WasserSiegel

-> Brücke: seidenfaden + LuftSiegel

-> EisSee: Bienenkorb

-> Stonehenge: bienenkorb + `Stachel`

-> Lichtung -> Irrgarten: BronzeSchlüssel + Silberschlüssel + Goldschlüssel + Waldsiegel + Feengold + Silberkraut + Stahlschlüssel

-> lichtung: stonehenge -> schrumpftrank

-> `Tsa Jelon`: Merina (aufgabe)

-> Stonehenge -> EisSee -> Winterfeehaus: Winterfee (Wasser + Winterblume)

-> EisSee -> Stonehende -> `Tsa Jelon`: Merina (?)

-> Haupthaus Siniver (Aufgabe)

-> `Bugrace`: `Rennschuhe`

-> Haupthaus: Astpaß

-> Stonehenge -> Lichtung -> Irrgarten ->Fort (MiniBoss): `Stimmgabel`

-> Irrgarten -> Lichtung -> Stonehenge -> Tsa Jelon -> Haus Mit Keller: Messer

-> Tsa Jelon: Aruna (Harfenseiten)

-> Stonehenge -> Brücke: Harfe reparieren

-> Irrgarten2: Fee ansprechen + Irrgarten leeren + Fee ansprechen

-> FrühlingfeeHaus: Blume des Frühling + Bogen

-> `Jelon` -> Temple des Sommers: Sommerblume (grün rot rot grün)

-> Jelon: Apydia (Bienenglas) + 5 Bienen + `Honig`

-> Lonicera's: Grückstrank

-> Jelon -> Bar -> Zauberstab

-> Horpach's: Rezept + Zahnrad

-> Bar: `Herbstblume`

-> Horpuach's: Elexier

(-> Palast Prinzessin aufgabe?)

-> Lonicera:'s: Aufgabe

-> Horpach's: Sieg

-> Lonicera's: Lichtglas

-> Jelon: Mondschein

-> Palast: Smile

-> Jelon -> `Irrgarten2` -> Sumpf -> `Sumpf 2` -> Sumpf 3 -> `Canyon`

## Canyon

## Tower

## Vulcano

## Ruins