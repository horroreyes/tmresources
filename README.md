# TechnoMage Resources

## AutoSplitter

The file AutoSplitter.asl is an AutoSplitter for [LiveSplit](https://livesplit.org/). It triggers a split without you pressing a key, based on the internal Memory-State of the game like the amount of balloons in your inventory or the map currently loaded.  
These triggers will trigger every time this condition is met, if you enter a house multiple times and this trigger is active, multiple splits will be triggered.  
The AutoSplitter does **not** differentiate which split is next, it just works like you pressing the *split*-Button and progresses to the next split.

### How to install the AutoSplitter

The TechnoMage-AutoSplitter is installed like every other AutoSplitter in LiveSplit:

1. Download the [ASL-File](https://bitbucket.org/horroreyes/tmresources/raw/master/AutoSplitter.asl) to a place where it can stay.
2. Open LiveSplit **with Admin-Rights** as it needs to inspect an other process' memory to use the AutoSplitter
3. Rightclick on your LiveSplit Window and click on *Edit Layout*  
   <img src="media/EditLayout.png" width="300" height="500">
4. Click on the bit **+** and select *control*->*Scriptable Auto Splitter* to add an AUtoSplitter to your Layout  
 <img src="media/AddSAS.png" width="500" height="300">

5. DoubleClick on the new Entry *Scriptable Auto Splitter* to open it's settings, click on Browse and select the downloaded file.  
   <img src="media/path.png" width="500" height="200">
   
6. Now LiveSplit should load the AutoSplitter and shows the settings where you can select different triggers etc.
7. The Options are
   1. **Start** Split on "Starting the game"
   2. **Split** Split on normal triggers selected in Advanced below
   3. **Reset** Reset the splits on game reset (not implemented yet)
   4. **Game Version: xyz** The recognized game version. I try to recognize the following Versions and use the corresponding memory-mappings
      1. v1.00
      2. v1.02 (not implemented yet)
      3. v1.02.NO_CD
      4. v1.04 (not implemented yet)
   5. **Advanced**
      1. **Split on level transitions** Split when completing a world like Dreamertown -> Steamertown
      2. **Dreamertown, Steamertown, ...** (de)select a whole level, none of the splits configured in that section will be triggered
      3. **Balloon, Shach, ...** the trigger, if selected the AutoSplitter will trigger a split when the corresponding event happens. Most of these events are getting of losing an inventory item (or getting a specific amount) or entering/leaving a map like *Graveyard -> Crypt Level 1* or *Dreamertown -> Rissens House* or *Rissens House -> Rissens Cellar Level 1*
      4. There is a default configuration selected, but you can (de)select everything you want.
      5. The options have Tooltips if you hover on them with your mouse.
8. Now you need to configure splits corresponding to your route, the AutoSplitter dows not uses a specific route, it just triggers the split on an event, you decide which split comes when.  
9. After successfully installing an AutoSplitter there will appear **no** splits automatically. You need to create them (or download them) by yourself. The AutoSplitter just "presses the *split*-Button for you"
   
### Known Problems
- Currently no bugs known, please report if you find some

## Maps
The folder **Maps** includes clean and enriched maps of the... maps. There are a lot missing right now.

## Map Routes
The folder **map_routes** includes graphical routes

## Images
The folder **images** includes all of the graphical resources I used for enriching the maps and as icons for LiveSplit in xcf-Format ([GIMP](https://www.gimp.org/)) to edit and png to use.

## CT-Files
The **.CT**-Files are memory-maps for [CheatEngine](https://www.cheatengine.org/), the tool I used to find the memory-mappings used in the TechnoMage-AutoSplitter. The mappings differ by Game-Version.

## Knowledge of TechnoMage Internals
Everything I found I wrote into the AutoSplitter-file like
- How to add things to the Inventory
- How to edit SaveGame-files
- Regeneration-Rates
- much more

## Licence
Except image-files, which show content of TechnoMage (TM), the following license is to apply:

  <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://bitbucket.org/horroreyes/tmresources/src/master/">TechnoMage Resources</a> by <span property="cc:attributionName">Horroreyes</span> is licensed under <a href="http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"></a></p> 