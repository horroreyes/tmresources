/*
This file explains all ingame-memory-cheating I found
and implements an autosplitter for livesplit

Savegame editing explained at the end of the file

*/


state("Technomage", "v1.02.NOCD")
{
	// Technomage.exe: 0x400000
	// int-Booleans (true if 1)
	// not in menu or options or TM-logo-animation
	int meta_InGame: "Technomage.exe", 0xB00660;
	// when inGame then this represents if the game is running or paused with esc, load/save etc
	int meta_Paused: "Technomage.exe", 0x194D44;
	
	int meta_Level: "Technomage.exe", 0x15ABAC;
	int meta_LevelGoal: "Technomage.exe", 0x428218;
	int meta_Map: "Technomage.exe", 0x45D700;
	int meta_XP: "Technomage.exe", 0x18F774;
	int meta_Strength: "Technomage.exe", 0x18F794;
	int meta_Mana: "Technomage.exe", 0x18F788;
	int meta_Mystic: "Technomage.exe", 0x18F798;
	int meta_Health: "Technomage.exe", 0x427BF8;
	int meta_West_East: "Technomage.exe", 0x427B0C;
	int meta_North_South: "Technomage.exe", 0x427B14;
	
	// Globals
	int weapon_Dagger: "Technomage.exe", 0x14FF38;
	int weapon_Sword: "Technomage.exe", 0x14FF44;
	int weapon_WarMace: "Technomage.exe", 0x14FF50;
	int weapon_WarHammer: "Technomage.exe", 0x14FF5C;
	int weapon_Axe: "Technomage.exe", 0x14FF68;
	int weapon_Bow: "Technomage.exe", 0x14FF74;
	int weapon_FireBow: "Technomage.exe", 0x14FF80;
	int weapon_WaterBow: "Technomage.exe", 0x14FF8C;
	int spell_Fireball: "Technomage.exe", 0x14FF98;
	int spell_IceClaw: "Technomage.exe", 0x14FFA4;
	int spell_VipersVortex: "Technomage.exe", 0x14FFB0;
	int spell_MeteorStorm: "Technomage.exe", 0x14FFBC;
	int spell_MagicLightning: "Technomage.exe", 0x14FFC8;
	int spell_MagicBattleShield: "Technomage.exe", 0x14FFD4;
	int spell_Earthquake: "Technomage.exe", 0x14FFE0;
	int defence_Ankh: "Technomage.exe", 0x15FFEC;
	int defence_RingofLife: "Technomage.exe", 0x14FFF8;
	int defence_MagicAmulett: "Technomage.exe", 0x150004;
	int defence_ChainMailCoat: "Technomage.exe", 0x150010;
	int defence_StoneskinBelt: "Technomage.exe", 0x15001C;
	int defence_MerlinsCape: "Technomage.exe", 0x150028;
	int defence_ExperienceAmulett: "Technomage.exe", 0x150034;
	int defence_GlovesofDexterity: "Technomage.exe", 0x150040;
	int tool_Bombs: "Technomage.exe", 0x15004C;
	int tool_Compass: "Technomage.exe", 0x150058;
	int tool_SpeedBoots: "Technomage.exe", 0x150064;
	int tool_GrapplingHook: "Technomage.exe", 0x150070;
	int greenCristal: "Technomage.exe", 0x1501A8;
	int yellowCristal: "Technomage.exe", 0x1501B4;
	int blueCristal: "Technomage.exe", 0x1501C0;
	int redCristal: "Technomage.exe", 0x1501CC;
	int whiteCristall: "Technomage.exe", 0x1501D8;
	
	// Global Inventory
	int chalk: "Technomage.exe", 0x150190;
	int gold: "Technomage.exe", 0x15019C;
	int torch: "Technomage.exe", 0x15016C;
	int steelKey: "Technomage.exe", 0x150154;
	int copperKey: "Technomage.exe", 0x150130;
	int silverKey: "Technomage.exe", 0x15013C;
	int goldKey: "Technomage.exe", 0x150148;
	int healthPotion: "Technomage.exe", 0x1500A0;
	int manaPotion: "Technomage.exe", 0x1500D0;
	int combiPotion: "Technomage.exe", 0x1500B8;
	int bigHealthPotion: "Technomage.exe", 0x1500AC;
	int bigManaPotion: "Technomage.exe", 0x1500DC;
	int bigCombiPotion: "Technomage.exe", 0x1500C4;
	int antiVenomHerb: "Technomage.exe", 0x1500E8;
	int antiVenomPotion: "Technomage.exe", 0x1500F4;
	int arrows: "Technomage.exe", 0x15007C;
	int fireArrows: "Technomage.exe", 0x150088;
	int waterArrows: "Technomage.exe", 0x150094;
	int mysticCube_Strength: "Technomage.exe", 0x150100;
	int magicCube_Intelligence: "Technomage.exe", 0x15010C;
	int magicCube_Constitution: "Technomage.exe", 0x150118;
	int magicCube_Mystic: "Technomage.exe", 0x150124;
	
	// Dreamertown inventory 1 if in inventory 0 if not (compass is 'in inventory' if you have it)
	int compass: "Technomage.exe", 0x150184;
	int balloon: "Technomage.exe", 0x1501FC;
	int books: "Technomage.exe", 0x1501F0;
	int shadeFern: "Technomage.exe", 0x150208;
	int rose: "Technomage.exe", 0x1501E4;
	int sandwitch: "Technomage.exe", 0x150214;
	int wine: "Technomage.exe", 0x150220;
	int sleepingDraught: "Technomage.exe", 0x15022C;
	int progress_TalkedToShach: "Technomage.exe", 0x407A5C;
	
	//Steamertown inventory
	int oilCan: "Technomage.exe", 0x1502B0;
	int cork: "Technomage.exe", 0x15025C;
	int stew: "Technomage.exe", 0x150280;
	int castorStew: "Technomage.exe", 0x15028C;
	int cat: "Technomage.exe", 0x150250;
	int castorOil: "Technomage.exe", 0x150268;
	int dynamite: "Technomage.exe", 0x150298;
	int loreLever: "Technomage.exe", 0x150238;
	int zantium: "Technomage.exe", 0x150244;
	int controlWheel: "Technomage.exe", 0x1502A4;
	
	//Fairy Forest inventory
	int sealHorse: "Technomage.exe", 0x1503A0;
	int moonGlassFull: "Technomage.exe", 0x150424;
	int moonGlassEmpty: "Technomage.exe", 0x150418;
	int antidotePotionSmile: "Technomage.exe", 0x15043C;
	int antidotePotion: "Technomage.exe", 0x150430;
	int flowerOfAutumn: "Technomage.exe", 0x150310;
	int luckPotion: "Technomage.exe", 0x15034C;
	int honey: "Technomage.exe", 0x150328;
	int magicwand: "Technomage.exe", 0x150340;
	int gearWheel: "Technomage.exe", 0x150460;
	int flowerOfSummer: "Technomage.exe", 0x150304;
	int bees: "Technomage.exe", 0x150388;
	int honeyGlass: "Technomage.exe", 0x150454;
	int flowerOfSpring: "Technomage.exe", 0x15031C;
	int harpstrings: "Technomage.exe", 0x1503C4;
	int tuningFork: "Technomage.exe", 0x150448;
	int oakBranch: "Technomage.exe", 0x150334;
	int shrinkingPotion: "Technomage.exe", 0x1503B8;
	int sealBird: "Technomage.exe", 0x1503AC;
	int beeSting: "Technomage.exe", 0x1503E8;
	int beeHives: "Technomage.exe", 0x150400;
	int fairyGold: "Technomage.exe", 0x1503F4;
	int knife: "Technomage.exe", 0x1503D0;
	int bucketFull: "Technomage.exe", 0x15037C;
	int bucketEmpty: "Technomage.exe", 0x150370;
	int silverweed: "Technomage.exe", 0x1502EC;
	int silkThread: "Technomage.exe", 0x1503DC;
	int sealFish: "Technomage.exe", 0x150394;
	int woodenMask: "Technomage.exe", 0x15040C;
	
	// Canyon inventory
	int redLens: "Technomage.exe", 0x1504A8;
	int blueLens: "Technomage.exe", 0x1504C0;
	int greenLens: "Technomage.exe", 0x1504B4;
	int greenEyeStone: "Technomage.exe", 0x150478;
	int blueEyeStone: "Technomage.exe", 0x150484;
	int yellowEyeStone: "Technomage.exe", 0x150490;
	int redEyeStone: "Technomage.exe", 0x15046C;
	int dragonHorn: "Technomage.exe", 0x1504CC;
	int lever: "Technomage.exe", 0x1504D8;
	int blackTeleporterStone: "Technomage.exe", 0x1502BC;
	
	// Tover Inventory
	int belladonna: "Technomage.exe", 0x150508;
	int spectacles: "Technomage.exe", 0x1504E4;
	int emptyUrn: "Technomage.exe", 0x150514;
	int urnWithAshes: "Technomage.exe", 0x150520;
	int blackPearl: "Technomage.exe", 0x1504FC;
	int book: "Technomage.exe", 0x15052C;
	
	// Vulcano Inventory
	int geode: "Technomage.exe", 0x1505B0;
	int cristal: "Technomage.exe", 0x150598;
	int bucket: "Technomage.exe", 0x150544;
	int shovel: "Technomage.exe", 0x15058C;
	int votingCoinRuisorne: "Technomage.exe", 0x15055C;
	int evidence: "Technomage.exe", 0x150580;
	int votingCoinCistone: "Technomage.exe", 0x150574;
	int metal: "Technomage.exe", 0x150550;
	int mead: "Technomage.exe", 0x1505F8;
	int book: "Technomage.exe", 0x1505BC;
	int egg: "Technomage.exe", 0x1505EC;
	int mushroom: "Technomage.exe", 0x150610;
	int dragonstooth: "Technomage.exe", 0x1505E0;
	int ring: "Technomage.exe", 0x15061C;
	int ironBall: "Technomage.exe", 0x150538;
	int votingCoinGambler: "Technomage.exe", 0x150568;
	int manaCristal: "Technomage.exe", 0x150634;
	int starDust: "Technomage.exe", 0x1505A4;
	
	// Ruins Inventory
	int fish: "Technomage.exe", 0x150640;
	int berries: "Technomage.exe", 0x15064C;
	int energyPearl: "Technomage.exe", 0x1506A0;
	int greenKeystone: "Technomage.exe", 0x150688;
	int yellowKeystone: "Technomage.exe", 0x150694;
	int whiteKeystone: "Technomage.exe", 0x150664;
	int redKeystone: "Technomage.exe", 0x150670;
	int blackKeystone: "Technomage.exe", 0x150658;
	int blueKeystone: "Technomage.exe", 0x15067C;
	int defenceSeal: "Technomage.exe", 0x1506C4;
	int combatSeal: "Technomage.exe", 0x1506B8;
	int magicSeal: "Technomage.exe", 0x1506D0;
	int mysticSeal: "Technomage.exe", 0x1506AC;

	// "Inventory"
	int inventory1: "Technomage.exe", 0x576440;
	int inventory2: "Technomage.exe", 0x576444;
	int inventory3: "Technomage.exe", 0x576448;
	int inventory4: "Technomage.exe", 0x57644C;
	int inventory5: "Technomage.exe", 0x576450;
	int inventory6: "Technomage.exe", 0x576454;
	int inventory7: "Technomage.exe", 0x576458;
	int inventory8: "Technomage.exe", 0x57645C;
	int inventory9: "Technomage.exe", 0x576460;
	int inventory10: "Technomage.exe", 0x576464;
	int inventory11: "Technomage.exe", 0x576468;
	int inventory12: "Technomage.exe", 0x57646C;
	int inventory13: "Technomage.exe", 0x576470;
	int inventory14: "Technomage.exe", 0x576474;
	int inventory15: "Technomage.exe", 0x576478;
	int inventory16: "Technomage.exe", 0x57647C;
	int inventory17: "Technomage.exe", 0x576480;
	int inventory18: "Technomage.exe", 0x576484;
	int inventory19: "Technomage.exe", 0x576488;
	int inventory20: "Technomage.exe", 0x57648C;
	int inventory21: "Technomage.exe", 0x576490;
	int inventory22: "Technomage.exe", 0x576494;
	int inventory23: "Technomage.exe", 0x576498;
	int inventory24: "Technomage.exe", 0x57649C;
	int inventory25: "Technomage.exe", 0x5764A0;
	int inventory26: "Technomage.exe", 0x5764A4;
	int inventory27: "Technomage.exe", 0x5764A8;
	
	
	
	// Level like 1 - Dreamertown, 2 - Steamertown, 3 - Hive, ... stays set in Menu!
	int level    		: "Technomage.exe", 0x15ABAC; // What level am I
	int altLevel 		: "Technomage.exe", 0x428218; // allternative Level value, changing teleports
	
	// Map values
	/*
	// 1 Dreamertown
	1: Dreamertown
	2: House Vesneggs inkl. cellar
	3: House Sengarn
	4: House Encende
	5: Melvins House
	6: School
	7: Wine Shop
	8: Inn
	9: Library
	10: High Council
	11: Risens House inkl. cellar
	12: Dreamertown Night	
	13: Empty House
	
	// 2 Steamertown
	1: Crypt 1	
	2: Crypt 2
	3: Crypt 3?
	4: Mine 1
	5: Mine 2	
	6: Mine 3
	7: Mine 4		
	8: Steamertown
	9: Graveyard
	10: Steamer Park
	11: Scrapyard
	12: Steamertown night
	13: Graveyard night		
	14: Unused Steamer Park night (yes there are dialogs for this)
	15: Unused Scrapyard night (wrong side of triggerzone)
	16: Ol'Raake's House
	17: Engeneers House
	18: Hospital
	19: Godon's House
	20: Smith
	21: Stm. House
	22: Miners shap
	23: Trader
	
	// 3 Hive
	1: Hive 1	
	2: Hive 2
	3: Hive 3
	4: Hive 4
	5: Hive 5	
	6: Hive 6
	7: Hive 7		
	8: Hive 8
	9: Hive 9
	10: Hive 10 (Endboss Spider)
	
	// 4 Fairy Forest
	1: Tsa Jelon
	2: Jelon
	3: Clearing (Shach Camp)
	4: Fairy Meadow (Maze with Keys)
	5: Goblon Fort
	6: Bridge
	7: Horpachs house & Lioncera & Palace (Jelon)
	8: Hause at Start in Jelon and SummerFairy and Puikkb with AutumnFairy
	9: Library (Winter fairy)
	10: Bug race
	11: Library (Spring Fairy)
	12: Error
	13: Error
	14: Error
	15: Error
	16: Unused? Empty House
	17: House with cellar (Tsa Jelon)
	18: Error
	19: Error
	20: Cellar Tsa Jelon
	21: House of Aruna (Harpist) Tsa Jelon
	22: House of Siniver (Tsa Jelon Majoress)
	23: House of Merina (Farming) Tsa Jelon
	24: Stonehendge
	25: Frozen Pond (Pool of ethernal ice)
	26: Dark Forest 1
	27: Dark Forest 2
	28: Dark Forest 3
	29: Maze with Spiders and Spring Fairy and Jelon
	
	// 5 Canyon
	1: Desert 1
	2: Desert 2: BehindRedApe with Talis House
	3: Desert 3: BehindBlueApe Divided Desert
	4: Desert 4: Route To Mine
	5: Error
	6: Mine 1
	7: Mine Endboss
	8: Error
	9: Mine 2
	10: Mine 3 BeetleRiddle
	11: Error
	12: Error
	13: Error
	14: Error
	15: Error
	16: RedApeCave, Talis House
	17: WarHammerCave (BlueApe2)
	18: StoneGuards
	19: RedApeCave 2
	20: BlueApeCave (Dagomar)
	
	// 6 Tower
	1: Tower 1
	2: Tower 2 ConferenceRooms
	3: Unused Tower-ritual-chamber
	4: Tower 3 Library
	5: Tower 4 Ritual Site/Crypt
	6: Tower 5 Laboratory
	7: Unused Rooftop
	8: Error
	9: Error
	10: Error
	11: Error
	12: Error
	13: SnowWorld
	14: Error
	15: Error
	16: Error
	17: Rooftop Dragonfight
	18: Error
	19: Error
	20: Error
	
	// 7 Vulcano
	1: Shachtown
	2: Vault 1 Below Shashtown
	3: Vault 2 Below Shashtown
	4: Vault 3
    5: Outside ShachTown
	6: OldOnesTown (Start)
	7: Error
	8: Vault 4 Rune
	
	// 8 Ruins
	1: ShieldGenerator
	2: Temple 2
	3: Test Magic
	4: Test Combat
	5: Test Mystic
	6: Beach
	7: Test Defence
	8: Error
	9: Ruins Exterior
	10: Temple 1
	11: Ruins Exterior 2
	
	*/
	int map      		: "Technomage.exe", 0x461748; // Where am I
	
	int altMap  		: "Technomage.exe", 0x45D700; // alternative map value, changing teleports
	int altMap2  		: "Technomage.exe", 0x15ABB0; // alternative map value, changing a little later then others
	
	/* The inventory is located from TechnoMage.exe+0x576440 to 0x5764A8 (last entry adress) in 27* 4 Bytes 
		Each entry contains the address of the Item it should contain -8, because the listed item-addresses are the amount of the given Item. before there is the Item information.
		
		--------------------
		Example: 
		Inventory Slot 1 should contain chalk: 
		Inventory Slot 1: TechnoMage.exe+0x576440
		Chalk (Amount): TechnoMage.exe+0x150190
		Chalk Base: TechnoMage.exe+0x150188
		Set TechnoMage.exe+0x576440 = 0x150188
		--------------------
	*/
	
}

state("Technomage", "v1.00")
{
	// Technomage.exe: 0x400000
	// int-Booleans (true if 1)
	// not in menu or options or TM-logo-animation
	
	// Every Inventory - address and globals is {v1.02-Address} - 0x46B10
	// meta is completly changed
	
        int meta_InGame: "Technomage.exe", 0xE74480;
        int meta_Paused: "Technomage.exe", 0x142E88;
		int meta_alive: "TechnoMage.exe", 0x142E90;
        int meta_Level: "Technomage.exe", 0x112ACC;
        int meta_LevelGoal: "Technomage.exe", 0x2BEFC8;
        int meta_Map: "Technomage.exe", 0x2BEFCC;
        int meta_XP: "Technomage.exe", 0x13FA34;
        int meta_Strength: "Technomage.exe", 0x148C84;
        int meta_Mana: "Technomage.exe", 0x13FA48;
        int meta_Mystic: "Technomage.exe", 0x13FA58;
        int meta_Health: "Technomage.exe", 0x2BEA38;
        int meta_West_East: "Technomage.exe", 0x2BE94C;
        int meta_North_South: "Technomage.exe", 0x2BE954;
        int meta_Talking: "Technomage.exe", 0x1463F00; // 255 if talking, 1 if not

        // Globals
        int weapon_Dagger: "Technomage.exe", 0x109428;
        int weapon_Sword: "Technomage.exe", 0x109434;
        int weapon_WarMace: "Technomage.exe", 0x109440;
        int weapon_WarHammer: "Technomage.exe", 0x10944C;
        int weapon_Axe: "Technomage.exe", 0x109458;
        int weapon_Bow: "Technomage.exe", 0x109464;
        int weapon_FireBow: "Technomage.exe", 0x109470;
        int weapon_WaterBow: "Technomage.exe", 0x10947C;
        int spell_Fireball: "Technomage.exe", 0x109488;
        int spell_IceClaw: "Technomage.exe", 0x109494;
        int spell_VipersVortex: "Technomage.exe", 0x1094A0;
        int spell_MeteorStorm: "Technomage.exe", 0x1094AC;
        int spell_MagicLightning: "Technomage.exe", 0x1094B8;
        int spell_MagicBattleShield: "Technomage.exe", 0x1094C4;
        int spell_Earthquake: "Technomage.exe", 0x1094D0;
        int defence_Ankh: "Technomage.exe", 0x1194DC;
        int defence_RingofLife: "Technomage.exe", 0x1094E8;
        int defence_MagicAmulett: "Technomage.exe", 0x1094F4;
        int defence_ChainMailCoat: "Technomage.exe", 0x109500;
        int defence_StoneskinBelt: "Technomage.exe", 0x10950C;
        int defence_MerlinsCape: "Technomage.exe", 0x109518;
        int defence_ExperienceAmulett: "Technomage.exe", 0x109524;
        int defence_GlovesofDexterity: "Technomage.exe", 0x109530;
        int tool_Bombs: "Technomage.exe", 0x10953C;
        int tool_Compass: "Technomage.exe", 0x109548;
        int tool_SpeedBoots: "Technomage.exe", 0x109554;
        int tool_GrapplingHook: "Technomage.exe", 0x109560;
        int greenCristal: "Technomage.exe", 0x109698; // Hive
        int yellowCristal: "Technomage.exe", 0x1096A4; // Fairy Forest
        int blueCristal: "Technomage.exe", 0x1096B0; // Canyon
        int redCristal: "Technomage.exe", 0x1096BC; // Tower
        int whiteCristall: "Technomage.exe", 0x1096C8; // Volcano

        // Global Inventory
        int chalk: "Technomage.exe", 0x109680;
        int gold: "Technomage.exe", 0x10968C;
        int torch: "Technomage.exe", 0x10965C;
        int steelKey: "Technomage.exe", 0x109644;
        int copperKey: "Technomage.exe", 0x109620;
        int silverKey: "Technomage.exe", 0x10962C;
        int goldKey: "Technomage.exe", 0x109638;
        int healthPotion: "Technomage.exe", 0x109590;
        int manaPotion: "Technomage.exe", 0x1095C0;
        int combiPotion: "Technomage.exe", 0x1095A8;
        int bigHealthPotion: "Technomage.exe", 0x10959C;
        int bigManaPotion: "Technomage.exe", 0x1095CC;
        int bigCombiPotion: "Technomage.exe", 0x1095B4;
        int antiVenomHerb: "Technomage.exe", 0x1095D8;
        int antiVenomPotion: "Technomage.exe", 0x1095E4;
        int arrows: "Technomage.exe", 0x10956C;
        int fireArrows: "Technomage.exe", 0x109578;
        int waterArrows: "Technomage.exe", 0x109584;
        int mysticCube_Strength: "Technomage.exe", 0x1095F0;
        int magicCube_Intelligence: "Technomage.exe", 0x1095FC;
        int magicCube_Constitution: "Technomage.exe", 0x109608;
        int magicCube_Mystic: "Technomage.exe", 0x109614;

        // Dreamertown inventory 1 if in inventory 0 if not (compass is 'in inventory' if you have it)
        int compass: "Technomage.exe", 0x109674;
        int balloon: "Technomage.exe", 0x1096EC;
        int books: "Technomage.exe", 0x1096E0;
        int shadeFern: "Technomage.exe", 0x1096F8;
        int rose: "Technomage.exe", 0x1096D4;
        int sandwitch: "Technomage.exe", 0x109704;
        int wine: "Technomage.exe", 0x109710;
        int sleepingDraught: "Technomage.exe", 0x10971C;
        int progress_TalkedToShach: "Technomage.exe", 0x2A3EBC; // 0 not talked, 1 talked, 2 payed

        //Steamertown inventory
        int oilCan: "Technomage.exe", 0x1097A0;
        int cork: "Technomage.exe", 0x10974C;
        int stew: "Technomage.exe", 0x109770;
        int castorStew: "Technomage.exe", 0x10977C;
        int cat: "Technomage.exe", 0x109740;
        int castorOil: "Technomage.exe", 0x109758;
        int dynamite: "Technomage.exe", 0x109788;
        int loreLever: "Technomage.exe", 0x109728;
        int zantium: "Technomage.exe", 0x109734;
        int controlWheel: "Technomage.exe", 0x109794;

        //Fairy Forest inventory
        int sealHorse: "Technomage.exe", 0x109890;
        int moonGlassFull: "Technomage.exe", 0x109914;
        int moonGlassEmpty: "Technomage.exe", 0x109908;
        int antidotePotionSmile: "Technomage.exe", 0x10992C;
        int antidotePotion: "Technomage.exe", 0x109920;
        int flowerOfAutumn: "Technomage.exe", 0x109800; // wrong
        int luckPotion: "Technomage.exe", 0x10983C;
        int honey: "Technomage.exe", 0x109818;
        int magicwand: "Technomage.exe", 0x109830;
        int gearWheel: "Technomage.exe", 0x109950;
        int flowerOfSummer: "Technomage.exe", 0x1097F4;
        int bees: "Technomage.exe", 0x109878;
        int honeyGlass: "Technomage.exe", 0x109944;
        int flowerOfSpring: "Technomage.exe", 0x10980C;
        int harpstrings: "Technomage.exe", 0x1098B4;
        int tuningFork: "Technomage.exe", 0x109938;
        int oakBranch: "Technomage.exe", 0x109824;
        int shrinkingPotion: "Technomage.exe", 0x1098A8;
        int sealBird: "Technomage.exe", 0x10989C;
        int beeSting: "Technomage.exe", 0x1098D8;
        int beeHives: "Technomage.exe", 0x1098F0;
        int fairyGold: "Technomage.exe", 0x1098E4;
        int knife: "Technomage.exe", 0x1098C0;
        int bucketFull: "Technomage.exe", 0x10986C;
        int bucketEmpty: "Technomage.exe", 0x109860;
        int silverweed: "Technomage.exe", 0x1097DC;
        int silkThread: "Technomage.exe", 0x1098CC;
        int sealFish: "Technomage.exe", 0x109884;
        int woodenMask: "Technomage.exe", 0x1098FC;

        // Canyon inventory
        int redLens: "Technomage.exe", 0x109998;
        int blueLens: "Technomage.exe", 0x1099B0;
        int greenLens: "Technomage.exe", 0x1099A4;
        int greenEyeStone: "Technomage.exe", 0x109968;
        int blueEyeStone: "Technomage.exe", 0x109974;
        int yellowEyeStone: "Technomage.exe", 0x109980;
        int redEyeStone: "Technomage.exe", 0x10995C;
        int dragonHorn: "Technomage.exe", 0x1099BC;
        int lever: "Technomage.exe", 0x1099C8;
        int blackTeleporterStone: "Technomage.exe", 0x1097AC;

        // Tover Inventory
        int belladonna: "Technomage.exe", 0x1099F8;
        int spectacles: "Technomage.exe", 0x1099D4;
        int emptyUrn: "Technomage.exe", 0x109A04;
        int urnWithAshes: "Technomage.exe", 0x109A10;
        int blackPearl: "Technomage.exe", 0x1099EC;
        int book: "Technomage.exe", 0x109A1C;

        // Vulcano Inventory
        int geode: "Technomage.exe", 0x109AA0;
        int cristal: "Technomage.exe", 0x109A88;
        int bucket: "Technomage.exe", 0x109A34;
        int shovel: "Technomage.exe", 0x109A7C;
        int votingCoinRuisorne: "Technomage.exe", 0x109A4C;
        int evidence: "Technomage.exe", 0x109A70;
        int votingCoinCistone: "Technomage.exe", 0x109A64;
        int metal: "Technomage.exe", 0x109A40;
        int mead: "Technomage.exe", 0x109AE8;
        int book: "Technomage.exe", 0x109AAC;
        int egg: "Technomage.exe", 0x109ADC;
        int mushroom: "Technomage.exe", 0x109B00;
        int dragonstooth: "Technomage.exe", 0x109AD0;
        int ring: "Technomage.exe", 0x109B0C;
        int ironBall: "Technomage.exe", 0x109A28;
        int votingCoinGambler: "Technomage.exe", 0x109A58;
        int manaCristal: "Technomage.exe", 0x109B24;
        int starDust: "Technomage.exe", 0x109A94;

        // Ruins Inventory
        int fish: "Technomage.exe", 0x109B30;
        int berries: "Technomage.exe", 0x109B3C;
        int energyPearl: "Technomage.exe", 0x109B90;
        int greenKeystone: "Technomage.exe", 0x109B78;
        int yellowKeystone: "Technomage.exe", 0x109B84;
        int whiteKeystone: "Technomage.exe", 0x109B54;
        int redKeystone: "Technomage.exe", 0x109B60;
        int blackKeystone: "Technomage.exe", 0x109B48;
        int blueKeystone: "Technomage.exe", 0x109B6C;
        int defenceSeal: "Technomage.exe", 0x109BB4;
        int combatSeal: "Technomage.exe", 0x109BA8;
        int magicSeal: "Technomage.exe", 0x109BC0;
        int mysticSeal: "Technomage.exe", 0x109B9C;
        int parrotGone: "Technomage.exe", 0x145ABC;

        // "Inventory"
        int inventory1: "Technomage.exe", 0x52F930;
        int inventory2: "Technomage.exe", 0x52F934;
        int inventory3: "Technomage.exe", 0x52F938;
        int inventory4: "Technomage.exe", 0x52F93C;
        int inventory5: "Technomage.exe", 0x52F940;
        int inventory6: "Technomage.exe", 0x52F944;
        int inventory7: "Technomage.exe", 0x52F948;
        int inventory8: "Technomage.exe", 0x52F94C;
        int inventory9: "Technomage.exe", 0x52F950;
        int inventory10: "Technomage.exe", 0x52F954;
        int inventory11: "Technomage.exe", 0x52F958;
        int inventory12: "Technomage.exe", 0x52F95C;
        int inventory13: "Technomage.exe", 0x52F960;
        int inventory14: "Technomage.exe", 0x52F964;
        int inventory15: "Technomage.exe", 0x52F968;
        int inventory16: "Technomage.exe", 0x52F96C;
        int inventory17: "Technomage.exe", 0x52F970;
        int inventory18: "Technomage.exe", 0x52F974;
        int inventory19: "Technomage.exe", 0x52F978;
        int inventory20: "Technomage.exe", 0x52F97C;
        int inventory21: "Technomage.exe", 0x52F980;
        int inventory22: "Technomage.exe", 0x52F984;
        int inventory23: "Technomage.exe", 0x52F988;
        int inventory24: "Technomage.exe", 0x52F98C;
        int inventory25: "Technomage.exe", 0x52F990;
        int inventory26: "Technomage.exe", 0x52F994;
        int inventory27: "Technomage.exe", 0x52F998;

        // Level like 1 - Dreamertown, 2 - Steamertown, 3 - Hive, ... stays set in Menu!
        int level               : "Technomage.exe", 0x11409C;
        int altLevel            : "Technomage.exe", 0x3E1708;

        int map                 : "Technomage.exe", 0x41AC38;

        int altMap              : "Technomage.exe", 0x416BF0;
        int altMap2             : "Technomage.exe", 0x1140A0;

}

startup
{
	
	// Should be sufficient
	refreshRate = 30;
	
	Action<int, string, string, string, bool> AddLevelSplit = (key, name, description, episode, check) => {
		settings.Add(episode+"_"+key, check, name, episode);
		settings.SetToolTip(episode+"_"+key, description);
	};

	settings.Add("levelSplits", true, "Split on level transitions");
	settings.Add("onlyFirst", true, "Only split on first entry");
	settings.SetToolTip("onlyFirst", "Resets only on Timer-Start by AutoSplitter or restart of LiveSplit");

	settings.Add("level1", true, "Dreamertown");
	AddLevelSplit(1, "Balloon", "Giving away the balloon", "level1", true);
	AddLevelSplit(6, "Shach", "Talked to the Shach", "level1", true);
	AddLevelSplit(2, "Rissen", "Entering Rissens house", "level1", true);
	AddLevelSplit(3, "Compass", "Geting the compass", "level1", true);
	AddLevelSplit(4, "Night", "Entering Dreamertown at night (after rat)", "level1", false);
	AddLevelSplit(5, "Sleeping Draught", "Getting the sleeping Draught", "level1", false);
	
	settings.Add("level2", true, "Steamertown");
	AddLevelSplit(1, "Enter Mine", "Entering the mine", "level2", true);
	AddLevelSplit(2, "Leave Mine", "Leaving the mine", "level2", true);
	AddLevelSplit(3, "Enter Crypt", "Entering the  crypt", "level2", true);
	AddLevelSplit(4, "Leave Crypt", "Leaving the  crypt (into Steamertown day)", "level2", true);
	AddLevelSplit(5, "Enter Steamerpark", "Entering Steamerpark", "level2", true);
	AddLevelSplit(6, "Getting zantium", "Getting zantium", "level2", false);
	AddLevelSplit(7, "Getting Control Wheel", "From Boss", "level2", false);
	AddLevelSplit(8, "Getting Lore Lever", "Getting Lore Lever", "level2", false);
	AddLevelSplit(9, "Getting golden Key", "Getting golden Key in Crypt", "level2", false);
	AddLevelSplit(10, "Getting Sword", "Getting the sword", "level2", false);
	AddLevelSplit(11, "Getting Fireball", "Getting the spell Fireball from granny", "level2", false);
	
	settings.Add("level3", true, "Hive");
	AddLevelSplit(2, "Level 2", "Entering level 2", "level3", true);
	AddLevelSplit(3, "Level 3", "Entering level 3", "level3", true);
	AddLevelSplit(4, "Level 4", "Entering level 4", "level3", true);
	AddLevelSplit(5, "Level 5", "Entering level 5", "level3", true);
	AddLevelSplit(6, "Level 6", "Entering level 6", "level3", true);
	AddLevelSplit(7, "Level 7", "Entering level 7", "level3", true);
	AddLevelSplit(8, "Level 8", "Entering level 8", "level3", true);
	AddLevelSplit(9, "Level 9", "Entering level 9", "level3", true);
	AddLevelSplit(10, "Level 10", "Entering level 10", "level3", true);
	AddLevelSplit(11, "Bomb", "Getting the bomb", "level3", false);
	AddLevelSplit(12, "Warmace", "Getting Warmace", "level3", false);
	AddLevelSplit(13, "Green Crystal", "Getting the green crystal", "level3", false);
	
	settings.Add("level4", true, "Fairy Forest");
	AddLevelSplit(1, "Bee Stink", "Getting the Bee Stink", "level4", true);
	AddLevelSplit(2, "Tsa Jelon", "Entering Tsa Jelon", "level4", true);
	AddLevelSplit(16, "Tsa Jelon every time", "Entering Tsa Jelon (triggers on every entry), has no effect of 'onlyFirts' is deactivated", "level4", true);
	AddLevelSplit(3, "Start Beaatle Race", "Starting the Beetle Race", "level4", true);
	AddLevelSplit(4, "End Beatle Race", "Ending the Beetle Race (no matter which ranking)", "level4", true);
	AddLevelSplit(5, "Jelon", "Entering Jelon (triggers on every entry)", "level4", true);
	AddLevelSplit(6, "Honey", "Getting Honey", "level4", true);
	AddLevelSplit(7, "Flower Of Autumn", "Getting the Flower of Autumn", "level4", true);
	AddLevelSplit(8, "Leave Jelon", "Leaving Jelon (triggers on every entry)", "level4", true);
	AddLevelSplit(9, "Dark Forest 2", "Entering the dark Forest Level 2", "level4", true);
	AddLevelSplit(10, "Start Boss", "Entering the Map of the Toad", "level4", true);
	AddLevelSplit(11, "Shrinking Potion", "Getting the Shrinking Potion", "level4", false);
	AddLevelSplit(12, "Speed Boots", "Getting speed boots", "level4", false);
	AddLevelSplit(13, "Honey", "Getting Honey", "level4", false);
	AddLevelSplit(14, "Yellow Crystal", "Gettingthe yellow Crystal", "level4", false);
	AddLevelSplit(15, "Smiled Antidote", "Getting the royal smile for the antidote potion", "level4", false);
	
	settings.Add("level5", true, "Canyon");
	AddLevelSplit(1, "Dagomar Cave", "Entering the Cave with your Mentor (Cave 1)", "level5", true);
	AddLevelSplit(2, "War Hammer", "Getting the Warhammer (Cave 2)", "level5", true);
	AddLevelSplit(3, "Meteroid Shower", "Getting the Spell Meteroid Shower (Cave 3)", "level5", true);
	AddLevelSplit(4, "Enter Mine", "Entering the Mine Level 1 (from outside)", "level5", true);
	AddLevelSplit(5, "Enter Mine 2", "Entering the Mine Level 2", "level5", true);
	AddLevelSplit(6, "Enter Mine 3", "Entering the Mine Level 3", "level5", true);
	AddLevelSplit(7, "Enter Mine Boss", "Entering the Mine", "level5", true);
	AddLevelSplit(8, "Red lens", "Getting the red lens", "level5", false);
	AddLevelSplit(9, "Green lens", "Getting the green lens", "level5", false);
	AddLevelSplit(10, "Blue lens", "Getting the blue lens", "level5", false);
	AddLevelSplit(11, "Blue Crystal", "Getting the blue Crystal", "level5", false);
	
	settings.Add("level6", true, "Tower");
	AddLevelSplit(1, "Enter Tower", "Entering the Tower", "level6", true);
	AddLevelSplit(2, "Enter Library", "Entering the Library", "level6", true);
	AddLevelSplit(3, "Enter Crypt", "Entering the Crypt", "level6", true);
	AddLevelSplit(4, "Enter Lab", "Entering the Laboratory", "level6", true);
	AddLevelSplit(5, "Enter Roof", "Entering the Roof (Boss)", "level6", true);
	AddLevelSplit(6, "Getting Belladonna", "Getting Belladonna", "level6", false);
	AddLevelSplit(7, "Getting glasses", "Getting the lost glasses", "level6", false);
	AddLevelSplit(8, "Getting urn", "Getting urn", "level6", false);
	AddLevelSplit(9, "Getting ash", "Getting ash", "level6", false);
	AddLevelSplit(10, "Getting red Crystal", "Getting the red Crystal", "level6", false);
	
	settings.Add("level7", true, "Vulcano");
	AddLevelSplit(1, "Enter Outside 1", "Entering Outside 1, outside Shachtown", "level7", true);
	AddLevelSplit(2, "Geode 7", "getting the 7th geode", "level7", false);
	AddLevelSplit(3, "Enter Shashtown", "Entering Shachtown", "level7", true);
	AddLevelSplit(4, "Enter Vault", "Entering Vault", "level7", true);
	AddLevelSplit(5, "Enter Vault 2", "Entering Vault 2", "level7", true);
	AddLevelSplit(6, "Enter Vault 3", "Entering Vault 3", "level7", true);
	AddLevelSplit(7, "Enter Vault Boss", "Entering Vault Boss", "level7", true);
	AddLevelSplit(8, "Getting Offensive Shield", "Getting the spell Offensive Shield", "level7", false);
	AddLevelSplit(9, "Getting white crystal", "Getting white crystal", "level7", false);
	AddLevelSplit(10,  "Getting Earthquake", "Getting the spell Earthquake", "level7", false);
	
	settings.Add("level8", true, "Ruins");
	AddLevelSplit(1, "Outside 1", "Entering Outside 1", "level8", true);
	AddLevelSplit(2, "Outside 2", "Entering Outside 2", "level8", true);
	AddLevelSplit(3, "Temple", "Entering Temple", "level8", true);
	AddLevelSplit(4, "Temple 2", "Entering Temple 2", "level8", true);
	AddLevelSplit(10, "Parrot free", "The Parrot leaves you, hopefully after opening the door (Bug in game: does NOT get resettet on savefile-load)", "level8", true);
	AddLevelSplit(5, "Get Defence Seal", "Get the Seal of Defence", "level8", true);
	AddLevelSplit(6, "Get Combat Seal", "Get the Seal of Combat", "level8", true);
	AddLevelSplit(7, "Get Magic Seal", "Get the Seal of Magic", "level8", true);
	AddLevelSplit(8, "Get Mystic Seal", "Get the Seal of Mystic", "level8", true);
	AddLevelSplit(9, "Endboss Dagomar", "Entering the fight with Dagomar", "level8", true);
	
	settings.Add("level9", true, "Dagomar");
	AddLevelSplit(1, "First Crystal", "Setting the first Crystal", "level9", true);
	AddLevelSplit(2, "Second Crystal", "Setting the second Crystal", "level9", true);
	AddLevelSplit(3, "Third Crystal", "Setting the third Crystal", "level9", true);
	AddLevelSplit(4, "Forth Crystal", "Setting the forth Crystal", "level9", true);
	AddLevelSplit(5, "Fifth Crystal", "Setting the fifth Crystal", "level9", true);
	AddLevelSplit(6, "End", "Win against Dagomar", "level9", true);
	
	vars.prevUpdateTime = -1;
	
	// Based on: https://github.com/NoTeefy/LiveSnips/blob/master/src/snippets/checksum(hashing)/checksum.asl
	Func<ProcessModuleWow64Safe, string> CalcModuleHash = (module) => {
		print("Calcuating hash of "+module.FileName);
		byte[] exeHashBytes = new byte[0];
		using (var sha = System.Security.Cryptography.MD5.Create())
		{
			using (var s = File.Open(module.FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				exeHashBytes = sha.ComputeHash(s);
			}
		}
		var hash = exeHashBytes.Select(x => x.ToString("X2")).Aggregate((a, b) => a + b);
		print("Hash: "+hash);
		return hash;
	};
	vars.CalcModuleHash = CalcModuleHash;

	var entries = new Dictionary<string, int>();
	vars.entries = entries;
	
	Func<int, int, int, bool, bool> enterMap = (level, fromMap, toMap, onlyFirst) => {
		// print("Entering "+fromMap + "->" + toMap + " in level " + level);
		var name = "entry_" + level + "_" + fromMap + "_" + toMap;
		var prevEntries = !entries.ContainsKey(name) ? 0 : entries[name];
		entries[name] = prevEntries +1;
		// print("Splitting: " + (!onlyFirst || entries[name]  == 1) + " as you already entered this " + entries[name]+ " times");
		return !onlyFirst || entries[name]  == 1;
	};
	vars.enterMap = enterMap;
	
	Action resetEntries = () => {
		print("Reset Counting");
		vars.entries.Clear();
	};
	vars.resetEntries = resetEntries;
	
	print("TechnoMage Autosplitter by Horroreyes successfully started");
}


init
{
	print(" try to get hash now");
	var module = modules.Single(x => String.Equals(x.ModuleName, "Technomage.EXE", StringComparison.OrdinalIgnoreCase));
	var moduleSize = module.ModuleMemorySize;
	print("Module Size: "+moduleSize+" "+module.ModuleName);
	var hash = vars.CalcModuleHash(module);

	if (hash == "94C5BE1D4E07AC5C68E7B5C99114CAB7") {// modules.First().ModuleMemorySize == 0x123456)
        version = "v1.02.NOCD";
	}  else if (hash == "B5171FAEC508458C497A81631E4EA448" ){
        version = "v1.00";
	} else if (hash == "BA0862BA2FC559A48C2ABDEB056C107A") {
		version = "v1.04";
	}
	vars.lastInGame = 0;
	return;
}

exit
{
	print("Exit start/done");
	timer.IsGameTimePaused = true;
}

update
{
	var timeSinceLastUpdate = Environment.TickCount - vars.prevUpdateTime;
	vars.prevUpdateTime = Environment.TickCount;
	
	/* if(current.inGame == 1){
		vars.lastInGame = Environment.TickCount;
	} else {
		vars.lastInGame = old.lastInGame;
	} */
	
}

start
{
	// print("Start timer? ingame:" + old.meta_InGame + "->" + current.meta_InGame + " Level: " + current.meta_Level + " Map: " + current.meta_Map);
	// Start is new Game (-> from menu, now ingame, in first level map melvins house, should not start in loads for Steamertown)
	if (old.meta_InGame == 0 && current.meta_InGame == 1 && current.meta_Level == 1 && current.meta_Map == 5) {
		vars.resetEntries();
		return true;
	}
	
	// Start on level progression (if level splits are activated)
	if (settings["levelSplits"] &&  current.meta_Level == old.meta_Level+1 && old.meta_Level != 0)
	{
		vars.resetEntries();
 		return true;
	}
	//print("Nooo!");
}

split
{
	// Detect Game beaten 
	if (settings["level9_6"] && current.meta_Level == 8 && current.meta_Map == 1 && old.meta_InGame == 1 && current.meta_InGame == 0)
	{
		return true;
	}
	if (current.meta_InGame == 0 )
	{
		return;
	}
	// next Level Split
	if (settings["levelSplits"] &&  current.meta_Level == old.meta_Level+1)
	{
		return true;
	}
	
	// Dreamertown splits
	if(settings["level1"] && current.meta_Level == 1){
		// Giving the balloon
		if (settings["level1_1"] && old.balloon == 1 && current.balloon == 0)
		{
			return true;
		}
		
		// Talked to the Shach
		if (settings["level1_6"] && old.progress_TalkedToShach == 0 && current.progress_TalkedToShach != 0) // might be 1 or 2
		{
			return true;
		}
		
		// Entering Rissen
		if (settings["level1_2"] && old.meta_Map == 1 && current.meta_Map == 11)
		{
			return vars.enterMap(1,1, 11, settings["onlyFirst"]);
		}
		
		// Getting compass
		if (settings["level1_3"] && old.compass == 0 && current.compass == 1)
		{
			return true;
		}
		
		// Leaving Rissen (night)
		if (settings["level1_4"] && old.meta_Map == 11 && current.meta_Map == 12)
		{
			return vars.enterMap(1,11, 12, settings["onlyFirst"]);
		}
		
		// Getting Sleeping Draught
		if (settings["level1_5"] && old.sleepingDraught == 0 && current.sleepingDraught == 1 )
		{
			return true;
		}
	}
	
	// Steamertown splits
	if(settings["level2"] && current.meta_Level == 2){
		
		// Enter Mine
		if (settings["level2_1"] && old.meta_Map == 8 && current.meta_Map == 4)
		{
			return vars.enterMap(2,8, 4, settings["onlyFirst"]);
		}
		
		// Leave Mine
		if (settings["level2_2"] && old.meta_Map == 4 && current.meta_Map == 8)
		{
			return vars.enterMap(2,4, 8, settings["onlyFirst"]);
		}
		
		// Enter Crypt
		if (settings["level2_3"] && old.meta_Map == 13 && current.meta_Map == 1)
		{
			return vars.enterMap(2,13, 1, settings["onlyFirst"]);
		}
		
		// Leave Crypt
		if (settings["level2_4"] && old.meta_Map == 1 && current.meta_Map == 9)
		{
			return vars.enterMap(2,1, 9, settings["onlyFirst"]);
		}
		
		// Enter Steamer Park
		if (settings["level2_5"] && old.meta_Map == 8 && current.meta_Map == 10)
		{
			return vars.enterMap(2,8, 10, settings["onlyFirst"]);
		}
		
		// Getting zantium
		if (settings["level2_6"] && old.zantium == 0 && current.zantium == 1)
		{
			return true;
		}
		
		// Getting Control Wheel
		if (settings["level2_7"] && old.controlWheel == 0 && current.controlWheel == 1)
		{
			return true;
		}
		
		// Getting Lore Lever
		if (settings["level2_8"] && old.loreLever == 0 && current.loreLever == 1)
		{
			return true;
		}
		
		// Getting golden Key
		if (settings["level2_9"] && old.goldenKey == 0 && current.goldenKey == 1)
		{
			return true;
		}
		
		// Getting Sword
		if (settings["level2_10"] && old.weapon_Sword == 0 && current.weapon_Sword == 1)
		{
			return true;
		}
		
		// Getting Sword
		if (settings["level2_11"] && old.spell_Fireball == 0 && current.spell_Fireball == 1)
		{
			return true;
		}
	}
	
	// Hive splits
	if(settings["level3"] && current.meta_Level == 3){
		if (settings["level3_2"] && old.meta_Map == 1 && current.meta_Map == 2)
		{
			return vars.enterMap(3,1, 2, settings["onlyFirst"]);
		}
		if (settings["level3_3"] && old.meta_Map == 2 && current.meta_Map == 3)
		{
			return vars.enterMap(3,2, 3, settings["onlyFirst"]);
		}
		if (settings["level3_4"] && old.meta_Map == 3 && current.meta_Map == 4)
		{
			return vars.enterMap(3,3, 4, settings["onlyFirst"]);
		}
		if (settings["level3_5"] && old.meta_Map == 4 && current.meta_Map == 5)
		{
			return vars.enterMap(3,4, 5, settings["onlyFirst"]);
		}
		if (settings["level3_6"] && old.meta_Map == 5 && current.meta_Map == 6)
		{
			return vars.enterMap(3,5, 6, settings["onlyFirst"]);
		}
		if (settings["level3_7"] && old.meta_Map == 6 && current.meta_Map == 7)
		{
			return vars.enterMap(3,6, 7, settings["onlyFirst"]);
		}
		if (settings["level3_8"] && old.meta_Map == 7 && current.meta_Map == 8)
		{
			return vars.enterMap(3,7, 8, settings["onlyFirst"]);
		}
		if (settings["level3_9"] && old.meta_Map == 8 && current.meta_Map == 9)
		{
			return vars.enterMap(3,8, 9, settings["onlyFirst"]);
		}
		if (settings["level3_10"] && old.meta_Map == 9 && current.meta_Map == 10)
		{
			return vars.enterMap(3,9, 10, settings["onlyFirst"]);
		}
		// Bomb
		if (settings["level3_11"] && old.tool_Bombs == 0 && current.tool_Bombs == 1)
		{
			return true;
		}
		// Warmace
		if (settings["level3_12"] && old.weapon_WarMace == 0 && current.weapon_WarMace == 1)
		{
			return true;
		}
		// Green crystal
		if (settings["level3_13"] && old.greenCristal == 0 && current.greenCristal == 1)
		{
			return true;
		}
	}
	
	// Fairy Forest splits
	if(settings["level4"] && current.meta_Level == 4){
		
		// Getting BeeStink
		if (settings["level4_1"] && old.beeSting == 0 && current.beeSting == 1)
		{
			return true;
		}
		
		// Enter Tsa Jelon (16 => trigger everytime)
		if ((settings["level4_2"] || settings["level4_16"]) && old.meta_Map == 1 && current.meta_Map == 24)
		{
			return settings["level4_16"] || vars.enterMap(4,1, 24, settings["onlyFirst"]);
		}
		
		// Start Beatle Race
		if (settings["level4_3"] && old.meta_Map == 1 && current.meta_Map == 10)
		{
			return vars.enterMap(4,1, 10, settings["onlyFirst"]);
		}

		// End Beaatle Race
		if (settings["level4_4"] && old.meta_Map == 10 && current.meta_Map == 1)
		{
			return vars.enterMap(4,10, 1, settings["onlyFirst"]);
		}

		// Enter Jelon
		if (settings["level4_5"] && old.meta_Map == 29 && current.meta_Map == 2)
		{
			return vars.enterMap(4,29, 2, settings["onlyFirst"]);
		}

		// Get Honey
		if (settings["level4_6"] && old.honey == 0 && current.honey == 1)
		{
			return true;
		}

		// Get Flower of Autumn
		if (settings["level4_7"] && old.flowerOfAutumn == 0 && current.flowerOfAutumn == 1)
		{
			return true;
		}

		// Leave Jelon
		if (settings["level4_8"] && old.meta_Map == 2 && current.meta_Map == 29)
		{
			return vars.enterMap(4,2, 29, settings["onlyFirst"]);
		}

		// Enter Dark Forest 2
		if (settings["level4_9"] && old.meta_Map == 26 && current.meta_Map == 27)
		{
			return vars.enterMap(4,26, 27, settings["onlyFirst"]);
		}

		// Start Boss
		if (settings["level4_10"] && old.meta_Map == 27 && current.meta_Map == 28)
		{
			return vars.enterMap(4,27, 28, settings["onlyFirst"]);
		}
		
		// Shrinking Potion
		if (settings["level4_11"] && old.shrinkingPotion == 0 && current.shrinkingPotion == 1)
		{
			return true;
		}
		// Speed Boots
		if (settings["level4_12"] && old.tool_SpeedBoots == 0 && current.tool_SpeedBoots == 1)
		{
			return true;
		}
		// Honey
		if (settings["level4_13"] && old.honey == 0 && current.honey == 1)
		{
			return true;
		}
		// Yellow Crysta
		if (settings["level4_14"] && old.yellowCristal == 0 && current.yellowCristal == 1)
		{
			return true;
		}
		// Smiled Antidote
		if (settings["level4_15"] && old.antidotePotionSmile == 0 && current.antidotePotionSmile == 1)
		{
			return true;
		}
		
	}
	
	// Canyon
	if(settings["level5"] && current.meta_Level == 5){
	// Dagomar Cave
		if (settings["level5_1"] && old.meta_Map == 2 && current.meta_Map == 20)
		{
			return vars.enterMap(5,2, 20, settings["onlyFirst"]);
		}
		// War Hammer
		if (settings["level5_2"] && old.weapon_WarHammer == 0 && current.weapon_WarHammer == 1)
		{
			return true;
		}
		// Meteroid Shower
		if (settings["level5_3"] && old.spell_MeteorStorm == 0 && current.spell_MeteorStorm == 1)
		{
			return true;
		}
		// Enter Mine
		if (settings["level5_4"] && old.meta_Map == 4 && current.meta_Map == 6)
		{
			return vars.enterMap(5,4, 6, settings["onlyFirst"]);
		}
		// Enter Mine 2
		if (settings["level5_5"] && old.meta_Map == 6 && current.meta_Map == 9)
		{
			return vars.enterMap(5,6, 9, settings["onlyFirst"]);
		}
		// Enter Mine 3
		if (settings["level5_6"] && old.meta_Map == 9 && current.meta_Map == 10)
		{
			return vars.enterMap(5,9, 10, settings["onlyFirst"]);
		}
		// Enter Mine Boss
		if (settings["level5_7"] && old.meta_Map == 10 && current.meta_Map == 7)
		{
			return vars.enterMap(5,10, 7, settings["onlyFirst"]);
		}
		
		// Red lens
		if (settings["level5_8"] && old.redLens == 0 && current.redLens == 1)
		{
			return true;
		}
		// Green lens
		if (settings["level5_9"] && old.greenLens == 0 && current.greenLens == 1)
		{
			return true;
		}
		// Blue lens
		if (settings["level5_10"] && old.blueLens == 0 && current.blueLens == 1)
		{
			return true;
		}
		// Blue Crystal
		if (settings["level5_11"] && old.blueCristal == 0 && current.blueCristal == 1)
		{
			return true;
		}
	}

	// Tower
	if(settings["level6"] && current.meta_Level == 6){
		// Enter Tower
		if (settings["level6_1"] && old.meta_Map == 13 && current.meta_Map == 1)
		{
			return vars.enterMap(6,13, 1, settings["onlyFirst"]);
		}
		// Enter Library
		if (settings["level6_2"] && old.meta_Map == 2 && current.meta_Map == 3)
		{
			return vars.enterMap(6,2, 3, settings["onlyFirst"]);
		}
		// Enter Crypt
		if (settings["level6_3"] && old.meta_Map == 1 && current.meta_Map == 5)
		{
			return vars.enterMap(6,1, 5, settings["onlyFirst"]);
		}
		// Enter Lab
		if (settings["level6_4"] && old.meta_Map == 4 && current.meta_Map == 6)
		{
			return vars.enterMap(6,4, 6, settings["onlyFirst"]);
		}
		// Enter Roof
		if (settings["level6_5"] && old.meta_Map == 6 && current.meta_Map == 17)
		{
			return vars.enterMap(6,6, 17, settings["onlyFirst"]);
		}
		
		// Getting Belladonna
		if (settings["level6_6"] && old.belladonna == 0 && current.belladonna == 1)
		{
			return true;
		}
		// Getting glasses
		if (settings["level6_7"] && old.glasses == 0 && current.glasses == 1)
		{
			return true;
		}
		// Getting urn
		if (settings["level6_8"] && old.urn == 0 && current.urn == 1)
		{
			return true;
		}
		// Getting ash
		if (settings["level6_9"] && old.urnWithAshes == 0 && current.urnWithAshes == 1)
		{
			return true;
		}
		// Forth Crystal (red)
		if (settings["level6_10"] && old.redCristal == 0 && current.redCristal == 1)
		{
			return true;
		}
	}
	
	// Vulcano
	if(settings["level7"] && current.meta_Level == 7){
		// Enter Outside 1
		if (settings["level7_1"] && old.meta_Map == 6 && current.meta_Map == 5)
		{
			return vars.enterMap(7,6, 5, settings["onlyFirst"]);
		}
		// Geode 7
		if (settings["level7_2"] && old.geode == 6 && current.geode == 7)
		{
			return true;
		}
		// Enter Shachtown
		if (settings["level7_3"] && old.meta_Map == 5 && current.meta_Map == 1)
		{
			return vars.enterMap(7,5, 1, settings["onlyFirst"]);
		}
		// Enter Vault
		if (settings["level7_4"] && old.meta_Map == 1 && current.meta_Map == 2)
		{
			return vars.enterMap(7,1, 2, settings["onlyFirst"]);
		}
		// Enter Vault 2
		if (settings["level7_5"] && old.meta_Map == 2 && current.meta_Map == 3)
		{
			return vars.enterMap(7,2, 3, settings["onlyFirst"]);
		}
		// Enter Vault 3
		if (settings["level7_6"] && old.meta_Map == 3 && current.meta_Map == 4)
		{
			return vars.enterMap(7,3, 4, settings["onlyFirst"]);
		}
		// Enter Vault Boss
		if (settings["level7_7"] && old.meta_Map == 4 && current.meta_Map == 8)
		{
			return vars.enterMap(7,4, 8, settings["onlyFirst"]);
		}
		// Getting Offensive Shield
		if (settings["level7_8"] && old.spell_MagicBattleShield == 0 && current.spell_MagicBattleShield == 1)
		{
			return true;
		}
		// Getting white crystal
		if (settings["level7_9"] && old.whiteCristall == 0 && current.whiteCristall == 1)
		{
			return true;
		}
		// Getting Earthquake
		if (settings["level7_10"] && old.spell_Earthquake == 0 && current.spell_Earthquake == 1)
		{
			return true;
		}
	}
	
	// Ruins
	if(settings["level8"] && current.meta_Level == 8){
		// Outside 1
		if (settings["level8_1"] && old.meta_Map == 6 && current.meta_Map == 9)
		{
			return vars.enterMap(8,6, 9, settings["onlyFirst"]);
		}
		// Outside 2
		if (settings["level8_2"] && old.meta_Map == 9 && current.meta_Map == 11)
		{
			return vars.enterMap(8,9, 11, settings["onlyFirst"]);
		}
		// Temple
		if (settings["level8_3"] && old.meta_Map == 11 && current.meta_Map == 10)
		{
			return vars.enterMap(8,11, 10, settings["onlyFirst"]);
		}
		// Temple 2
		if (settings["level8_4"] && old.meta_Map == 10 && current.meta_Map == 2)
		{
			return vars.enterMap(8,10, 2, settings["onlyFirst"]);
		}
		// Free Parrot
		if (settings["level8_10"] && old.parrotGone == 0 && current.parrotGone == 1)
		{
			return true;
		}
		// Get Defence Seal
		if (settings["level8_5"] && old.defenceSeal == 0 && current.defenceSeal == 1)
		{
			return true;
		}
		// Get Combat Seal
		if (settings["level8_6"] && old.combatSeal == 0 && current.combatSeal == 1)
		{
			return true;
		}
		// Get Magic Seal
		if (settings["level8_7"] && old.magicSeal == 0 && current.magicSeal == 1)
		{
			return true;
		}
		// Get Mystic Seal
		if (settings["level8_8"] && old.mysticSeal == 0 && current.mysticSeal == 1)
		{
			return true;
		}
		// Endboss Dagomar
		if (settings["level8_9"] && old.meta_Map == 2 && current.meta_Map == 1)
		{
			return vars.enterMap(8,2, 1, settings["onlyFirst"]);
		}
	}
	
	// Dagomar Fight
	if(settings["level9"] && current.meta_Level == 8){
		// First Crystal
		if (settings["level9_1"] && old.greenCristal == 1 && current.greenCristal == 0)
		{
			return true;
		}
		// Second Crystal
		if (settings["level9_2"] && old.yellowCristal == 1 && current.yellowCristal == 0)
		{
			return true;
		}
		// Third Crystal
		if (settings["level9_3"] && old.blueCristal == 1 && current.blueCristal == 0)
		{
			return true;
		}
		// Forth Crystal
		if (settings["level9_4"] && old.redCristal == 1 && current.redCristal == 0)
		{
			return true;
		}
	}
}

isLoading
{
	return current.meta_InGame == 0 || current.meta_Paused == 1;
}

reset
{
	/*if(current.inGame == 0 && (current.lastInGame - Environment.TickCount) > 500){
		return true;
	}*/
}

/*
Health, Mana, XP and Stuff
XP:
Level 1: 10000
Level 2: 22000   	+ 12000
Level 3: 35000		+ 13000		+ 1000
Level 4: 51000		+ 16000		+ 3000
Level 5: 69000		+ 18000		+ 2000
Level 6: 90000		+ 21000		+ 3000
Level 7: 115000		+ 25000		+ 4000
Level 8: 143000		+ 28000		+ 3000
Level 9: 175000		+ 32000		+ 4000
Level 10: 215000	+ 40000		+ 8000
Level 11: 258000	+ 43000		+ 3000
Level 12: 310000	+ 52000		+ 9000
Level 13: 370000	+ 60000		+ 8000
Level 14: 440000	+ 70000		+ 10000
Level 15: 520000	+ 80000		+ 10000
Level 16: 610000	+ 90000		+ 10000
Level 17: 720000	+ 110000	+ 20000
Level 18: 850000	+ 130000	+ 20000
Level 19: 1000000	+ 150000	+ 20000

Health: 10 + (5 * Constitution) + (6 * Level)
Mana: 5 + (10 * Intelligence)
Strength: 1 + (256*Strength) + Level

Potions:
Small Potion: 25% flooring, 33% flooring with Mys 14 Effective Potions
orbs: 10%iq

Weapons:
Dagger: 1-4
Sword: 2-8
Mace: 0-1
Hammer: 0
Axe: 6-7+

Mana regen: ~ 5%/15s
Health regen: ~ 2,5%/15s
Health regen with Ring of life: ~ 5%/15s

Enemies:
Rat: HP: 3-5, Dmg: 1-2, XP: 125
Bat: XP: 100
Troll (mine): XP: 166
Firelizard: XP: 100
Flies (Hive): XP: 130
Rollers (Hive): XP: 134
Spider (Hive): XP: 104

*/
/*

Savegame editing (from http://www.cheatbook.de/files/techmage.htm

In a saved game in the directory SAVES under the installed directory 
(filenames "TMN_01.TSG" through "TMN_10.TSG"), are the following addresses: 

To enable all skill items (bow, fireball, earthquake, etc.), the following 
addresses should be set to 01 hex unless a non-zero value is already there: 

4b18,4b1c
4b20,4b24,4b28,4b2c
4b30,4b34,4b38,4b3c
4b40,4b44,4b48,4b4c
4b50,4b54,4b58,4b5c
4b60,4b64,4b68,4b6c
4b70,4b74,4b78,4b7c 

4f14-4f49 - 27 Inventory slots, 2 bytes per slot.
If the slot is empty, the values are ff ff hex
If the slot is occupied, the first byte is the item code (below), 
the second byte is 00 hex. 

Inventory Item Codes (in hex) 

value - description (hex address of the number of the item the player has)
-=Common=-
1b - arrows (4b80 - set to ff hex for infinite arrows)
1c - fire arrows (4b84 - set to ff hex for infinite fire arrows) 
1d - water arrows (4b88 - set to ff hex for infinite water arrows)
1e - small healing potion (4b8c) 
1f - large healing potion (4b90) 
20 - small combined potion (4b94) 
21 - large combined potion (4b98) 
22 - small mana potion (4b9c) 
23 - large mana potion (4ba0) 
24 - anti-venom herb (4ba4) 
25 - anti-venom potion (4ba8) 
26 - magic cube (strength) (4bac) 
27 - magic cube (intelligence) (4bb0) 
28 - magic cube (constitution) (4bb4) 
29 - magic cube (mystic) (4bb8) 
2a - copper key (4bbc) 
2b - silver key (4bc0) 
2c - gold key (4bc4) 
2d - steel key (4bc8) 
2f - torch (4bd0) 
31 - compass (N/A) 
32 - chalk (4bdc)
Dreamertown:
39 - a rose (4bf8)
3a - Mrs. Sengarn's books (4bfc)
3b - a balloon, somewhat deflated (4c00)
3c - shade fern (4c04)
3d - bread and butter (4c08)
3e - Master Bacchus' best booze (4c0c)
3f - sleeping draught (4c10)
Steamertown:
40 - crowbar (4c14)
41 - lump of zantium (4c18)
42 - kitten (4c1c)
43 - cork (4c20)
44 - castor oil (4c24)
45 - empty stew plate (4c28)
46 - stew (4c2c)
47 - stew with castor oil (4c30)
48 - dynamite (4c34)
49 - control wheel (4c38)
4a - oil can (4c3c)
The Hive:
4b - black module (4c40)
4c - blue module (4c44)
4d - green module (4c48)
4e - red module (4c4c)
Fairy Forest:
4f - silverweed (4c50)
50 - blue ice flower (4c54)
51 - red sunflower (4c58)
52 - dark green flower (4c5c)
53 - light green spring flower (4c60)
54 - honeycomb (4c64 - adds this value with value in other honeycomb) 
55 - branch from an old oak (4c68) 
56 - Horpach's magic wand (4c6c) 
57 - good luck potion (4c70) 
58 - yellow crystal (4c74) 
59 - honeycomb (4c78) 
5a - empty bucket (4c7c) 
5b - bucket of fresh water (4c80) 
5c - captured bee (4c84) 
5d - fish seal (4c88) 
5e - horse seal (4c8c) 
5f - bird seal (4c90) 
60 - shrinking potion (4c94) 
61 - harp strings (4c98) 
62 - knife (4c9c) 
63 - silk thread (4ca0)
64 - bee sting (4ca4) 
65 - fairy gold (4ca8) 
66 - bee basket (4cac) 
67 - wooden mask (pass) (4cb0) 
68 - ray glass (4cb4) 
69 - moonbeams in a ray glass (4cb8) 
6a - antidote potion (4cbc) 
6b - antidote potion blessed by a royal smile (4cc0) 
6c - tuning fork (4cc4) 
6d - honey glass (for catching bees) (4cc8) 
6e - gear wheel (4ccc)
Canyon:
6f - red eye stone (4cd0)
70 - green eye stone (4cd4)
71 - blue eye stone (4cd8)
72 - yellow eye stone (4cdc)
74 - red lens (4ce4)
75 - green lens (4ce8)
76 - blue lens (4cec)
77 - Cahoch-Ran's horn (4cf0)
78 - lever (4cf4)
79 - old spectacles (4cf8)
Tower:
7a - black pearl (4d00)
7b - belladonna (4d04)
7c - empty urn (4d08)
7d - urn filled with ashes (4d0c)
7e - book on ghost conjuring (4d10)
Volcano:
80 - blue iron ball (4d14)
81 - bucket (4d18)
82 - lump of metal (4d1c)
83 - Ruisorne's voting coin (4d20)
84 - gambler's voting coin (4d24)
85 - Cistorne's voting coin (4d28)
86 - the evidence (4d2c)
87 - shovel (4d30)
88 - crystal (4d34)
89 - stardust (4d38)
8a - geode (4d3c)
8b - book (4d40)
8c - amulet (4d44)
8d - crystal (4d48)
8e - dragon's tooth (4d4c)
8f - egg from the hive (4d50)
90 - goblet of mead (4d54)
91 - nut (4d58)
92 - mushroom (4d5c)
93 - ring (4d60)
94 - skull (4d64)
95 - shard of mana crystal (4d68)
Ruins:
96 - fish (4d6c)
97 - berries (4d70)
98 - black keystone (4d74)
99 - white keystone (4d78)
9a - red keystone (4d7c)
9b - blue keystone (4d80)
9c - green keystone (4d84)
9d - yellow keystone (4d88)
9e - energy pearl (4d8c)
9f - seal of mysticism (4d90)
a0 - seal of combat (4d94)
a1 - seal of defence (4d98)
a2 - seal of magic (4d9c) 

Note 1: Many of these items will only display properly in the inventory list 
        depending on what level is being played. 

Note 2: Adding the above values to the inventory area is only the first step. To 
        have the game acknowledge that the item displayed in the inventory is 
        actually there, the hex address in parenthesis has to have a non-zero value.

4be0-2 - Gold; Max 3f 42 0f hex ($999,999)
4f51-2 and 4f59-a - Location player is standing. 
Playing with these values can actually relocate the player to a 
desired location. Subtracting from 4f51-2 moves the player to the 
west in the game. Adding to 4f51-2 moves the player to the east. 
Subtracting from 4f59-a moves the player north and adding moves 
the player south. Try adding (or subtracting) one or two hex values 
to (or from) the leftmost bit of address 4f51 (or 4f59). 
So a hex a5 would become hex b5 if adding and hex 95 if 
subtracting. If that value is already very near the maximum 
of ff, then make the leftmost bit a 0 and add 1 to the righmost 
bit of address 4f52 (or 4f5a). 
So hex f5 01 would become hex 05 02. Quite often you will end up 
on top of a wall separating one room from another. Simply walk to 
where you wanted to be and drop down. Note: 
The corresponding *memory* locations are 827b0d-e for east-west 
and 827b15-6 for north-south. 
 
A program like Magic Trainer Creator can be used to manipulate 
these locations. This is especially useful in the final battle 
when saving the game is not permitted. 

73ad - Strength skill(STR); Max 28 hex
73ae - Intelligence skill(INT); Max 28 hex
73af - Constitution skill(CON); Max 28 hex
73b0 - Mystical skill(MYS); Max 28 hex
*/
